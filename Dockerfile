FROM java:8
ADD target/watson.jar watson.jar
ENTRYPOINT ["java","-jar","watson.jar"]