package com.wavelabs.controller;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ibm.watson.developer_cloud.document_conversion.v1.DocumentConversion;
import com.ibm.watson.developer_cloud.document_conversion.v1.model.Answers;

@RestController
public class DocumentConvertrController {

	@RequestMapping(value="/convert", method=RequestMethod.POST)
	@CrossOrigin
	public ResponseEntity convertDocument(@RequestParam("file") MultipartFile uploadfile, @RequestParam("type") String type) throws IOException {
		DocumentConversion service = new DocumentConversion("2015-12-01");
		service.setUsernameAndPassword("be86e2c8-a0b1-442b-a1b2-9542efd25ebe", "ikpTlzLUfT7X");
		String filename = uploadfile.getOriginalFilename();
	    String directory = "./";
	    String filepath = Paths.get(directory, filename).toString();
	    // Save the file locally
	    BufferedOutputStream stream =
	        new BufferedOutputStream(new FileOutputStream(new File(filepath)));
	    stream.write(uploadfile.getBytes());
	    stream.close();
		//uploadfile.transferTo(file);
	    System.out.println(filename);
		File doc = new File("./"+filename);
		String response="";
		if(type.equalsIgnoreCase("html")) {
			response = service.convertDocumentToHTML(doc).execute();
			System.out.println(response);
			return ResponseEntity.status(200).body(response);
		}
		else if(type.equalsIgnoreCase("text")) {
			String textResponse=service.convertDocumentToText(doc).execute();
			//Answers textResponse = service.convertDocumentToAnswer(doc).execute();
			System.out.println(textResponse);
			return ResponseEntity.status(200).body(textResponse);
		}
		else if(type.equalsIgnoreCase("json")) {
			Answers htmlToAnswers = service.convertDocumentToAnswer(doc).execute();
			System.out.println(htmlToAnswers);
			return ResponseEntity.status(200).body(htmlToAnswers);
		}
		//Answers htmlToAnswers = service.convertDocumentToAnswer(doc).execute();
		//String response = service.convertDocumentToHTML(doc).execute();
		return ResponseEntity.status(200).body(response);
	}
}
