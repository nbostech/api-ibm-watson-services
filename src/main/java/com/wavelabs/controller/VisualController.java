package com.wavelabs.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ibm.watson.developer_cloud.visual_recognition.v3.VisualRecognition;
import com.ibm.watson.developer_cloud.visual_recognition.v3.model.ClassifyImagesOptions;
import com.ibm.watson.developer_cloud.visual_recognition.v3.model.VisualClassification;
/**
 * 
 * @author gopikrishnag
 *
 */
@RestController

public class VisualController {
	public static final String uploadingdir = System.getProperty("user.dir") + "/uploadingdir/";
	private static int i=1;
	@RequestMapping(method = RequestMethod.POST, value = "/img")
	@CrossOrigin
	public String test(@RequestParam("file") MultipartFile uploadfile) throws IllegalStateException, IOException {
		VisualRecognition service = new VisualRecognition(VisualRecognition.VERSION_DATE_2016_05_20);
		service.setApiKey("91d13e4bacd72f3ea01ed984f668706cc4a157cb");
		String filename = uploadfile.getOriginalFilename();
	    String directory = "./";
	    String filepath = Paths.get(directory, filename).toString();
	    // Save the file locally
	    BufferedOutputStream stream =
	        new BufferedOutputStream(new FileOutputStream(new File(filepath)));
	    stream.write(uploadfile.getBytes());
	    stream.close();
		//uploadfile.transferTo(file);
	    System.out.println(filename);
		ClassifyImagesOptions options = new ClassifyImagesOptions.Builder()
				.images(new File("./"+filename)).build();
		options.classifierIds();
		VisualClassification result = service.classify(options).execute();
		return result.toString();
	}
}
