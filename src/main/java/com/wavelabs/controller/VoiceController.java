package com.wavelabs.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ibm.watson.developer_cloud.http.ServiceCall;
import com.ibm.watson.developer_cloud.text_to_speech.v1.TextToSpeech;
import com.ibm.watson.developer_cloud.text_to_speech.v1.model.Phoneme;
import com.ibm.watson.developer_cloud.text_to_speech.v1.model.Pronunciation;
import com.ibm.watson.developer_cloud.text_to_speech.v1.model.Voice;
/**
 * 
 * @author gopikrishnag
 *
 */
@RestController
public class VoiceController {

	@RequestMapping(method = RequestMethod.GET, value = "/voice/translate")
	public void textToSpeech(@Param("text") String text) {
		TextToSpeech service = new TextToSpeech();
		service.setUsernameAndPassword("92addf4d-add9-4e7b-8ee6-abd066a1c576", "2QBUt1EYWnev");
		List<Voice> voices = service.getVoices().execute();
		List<ServiceCall<Pronunciation>> list = new ArrayList<>();
		for (Voice voice : voices) {
			ServiceCall<Pronunciation> pts = service.getPronunciation(text, voice, Phoneme.IPA);
			System.out.println(pts.execute());
		}
	}
}
