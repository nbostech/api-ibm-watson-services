package com.wavelabs.controller;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ibm.watson.developer_cloud.conversation.v1.ConversationService;
import com.ibm.watson.developer_cloud.conversation.v1.model.InputData;
import com.ibm.watson.developer_cloud.conversation.v1.model.MessageOptions;
import com.ibm.watson.developer_cloud.conversation.v1.model.MessageResponse;
/**
 * 
 * @author gopikrishnag
 *
 */
@RestController
public class ConversationController {

	@RequestMapping("/chatbot/{message}")
	@CrossOrigin
	public ResponseEntity<MessageResponse> getReplay(@PathVariable("message") String message) {
		ConversationService service = new ConversationService(ConversationService.VERSION_DATE_2017_02_03);
		service.setUsernameAndPassword("5df66b2d-5e0b-4db7-861e-8fb69e501a4f", "h35gRX2e8gLW");

		InputData input = new InputData.Builder(message).build();
		MessageOptions options = new MessageOptions.Builder("f387ecdf-d5ec-4f4a-bf06-85f58cfa1512").input(input)
				.build();
		MessageResponse response = service.message(options).execute();
		System.out.println(response);
		return ResponseEntity.status(200).body(response);
		/*
		 * // first message MessageRequest newMessage = new
		 * MessageRequest.Builder() .input(new InputData.Builder("Hi").build())
		 * .context(context) .build();
		 * 
		 * MessageResponse response1 =
		 * service.message("dcff465b-524c-4c65-b3e5-62c52633d4b2",
		 * newMessage).execute();
		 * 
		 * // second message newMessage = new MessageRequest.Builder()
		 * .input(new InputData.Builder("Hackthon").build())
		 * .context(response.getContext()) // output context from the first
		 * message .build();
		 * 
		 * response = service.message("dcff465b-524c-4c65-b3e5-62c52633d4b2",
		 * newMessage).execute();
		 * 
		 * System.out.println(response);
		 */

	}
	
	@RequestMapping("/location/{message}")
	public ResponseEntity<MessageResponse> getReplayFromMap(@PathVariable("message") String message) {
		ConversationService service = new ConversationService(ConversationService.VERSION_DATE_2017_02_03);
		service.setUsernameAndPassword("5df66b2d-5e0b-4db7-861e-8fb69e501a4f", "h35gRX2e8gLW");
		InputData input = new InputData.Builder(message).build();
		MessageOptions options = new MessageOptions.Builder("69ec0843-6988-423a-a453-f0aebaaa3ed5").input(input)
				.build();
		MessageResponse response = service.message(options).execute();
		System.out.println(response);
		return ResponseEntity.status(200).body(response);
	}
	
	/*@RequestMapping("/job/{message}")
	public ResponseEntity getJobInformation(@PathVariable("message") String message) {
		ConversationService service = new ConversationService(ConversationService.VERSION_DATE_2017_02_03);
		service.setUsernameAndPassword("97ebc382-80a1-4d44-8e42-0a619cb4f142", "jynznSxDud6t");
		InputData input = new InputData.Builder(message).build();
		MessageOptions options = new MessageOptions.Builder("a717996d-a87f-4a96-97e9-a90885072aee").input(input)
				.build();
		MessageResponse response = service.message(options).execute();
		System.out.println(response);
		return ResponseEntity.status(200).body(response);
	}*/
}
