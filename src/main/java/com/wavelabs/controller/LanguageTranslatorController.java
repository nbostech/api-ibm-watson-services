package com.wavelabs.controller;

import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ibm.watson.developer_cloud.language_translator.v2.LanguageTranslator;
import com.ibm.watson.developer_cloud.language_translator.v2.model.Language;
import com.ibm.watson.developer_cloud.language_translator.v2.model.TranslationResult;
/**
 * 
 * @author gopikrishnag
 *
 */
@RestController
public class LanguageTranslatorController {
	@RequestMapping(method = RequestMethod.GET, value = "/translate")
	@CrossOrigin
	public ResponseEntity<String> translateFromEnglishToTelugu(@Param("text") String text) {
		LanguageTranslator service = new LanguageTranslator();
		service.setUsernameAndPassword("df215ede-6a6c-42ac-bbed-a6cda23c33cb", "sX14E1cJiF8u");
		TranslationResult translationResult = service.translate(text, Language.ENGLISH, Language.SPANISH).execute();
		String teluguText = translationResult.getFirstTranslation();
		return ResponseEntity.status(200).body(teluguText);
	}
}
